﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestTubeRecord
{
    /// <summary>
    /// 试管位置记录
    /// </summary>
    internal class TubeOrderIndex
    {
        public TubeOrderIndex(int xIndex, int yIndex)
        {
            XIndex = xIndex;
            YIndex = yIndex;
        }

        public int XIndex { get; set; }
        public int YIndex { get; set; }
    }
}
