﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace TestTubeRecord
{
    /// <summary>
    /// 重绘时不闪烁
    /// </summary>
    internal class DrawPanel : System.Windows.Forms.Panel
    {
        public DrawPanel()
        {
            SetStyle(ControlStyles.UserPaint |
                   ControlStyles.AllPaintingInWmPaint |
                   ControlStyles.OptimizedDoubleBuffer |
                   ControlStyles.ResizeRedraw |
                   ControlStyles.SupportsTransparentBackColor, true);
        }
    }
}
