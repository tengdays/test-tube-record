﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestTubeRecord
{
    public class Record
    {
        public ulong Id { get; set; }
        public string Code { get; set; }

        public int PositionX { get; set; }

        public int PositionY { get; set; }
        public DateTime Time { get; set; }

    }
}
