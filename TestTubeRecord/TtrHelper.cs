﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestTubeRecord
{
    /// <summary>
    /// TestTubeRecord 帮助类
    /// </summary>
    public static class TtrHelper
    {
        /// <summary>
        /// Excel 列名（从 1 开始，所以 0 的位置留空）
        /// </summary>
        private static readonly string[] excelColumnName = new[] { "", "A", "B", "C", "D", "E", "F", "G" };

        /// <summary>
        /// 获取 Excel 列表名称，并自动 + 1
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string GetExcelColumnName(this int index)
        {
            var columName = excelColumnName[index];
            return columName;
        }

        private static List<Form> SubForms { get; set; } = new List<Form>();

        /// <summary>
        /// 管理子窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        internal static Form GetSubForm<T>()
            where T : Form
        {
            var type = typeof(T);
            var form = SubForms.FirstOrDefault(z => z.GetType()?.Name == type.Name);

            if (form == null || form.IsDisposed)
            {
                if (form != null && form.IsDisposed)
                {
                    SubForms.Remove(form);
                }

                form = Senparc.CO2NET.Helpers.ReflectionHelper.CreateInstance<T>(type.FullName, type.Assembly.FullName);
                SubForms.Add(form);
            }

            return form;
        }

        /// <summary>
        /// 获取版本号
        /// </summary>
        internal static string GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }


    }
}
