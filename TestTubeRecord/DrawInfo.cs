﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TestTubeRecord
{
    /// <summary>
    /// 绘画信息
    /// </summary>
    internal class DrawInfo
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Brush Brush { get; set; }
        public Pen Pen { get; set; }
        public int Radius { get; set; }
        public DrawType DrawType { get; set; }

        public DrawInfo(int x, int y, Brush brush, int radius)
        {
            X = x;
            Y = y;
            Brush = brush;
            Radius = radius;
            DrawType = DrawType.Fill;
        }

        public DrawInfo(int x, int y, Pen pen, int radius)
        {
            X = x;
            Y = y;
            Pen = pen;
            Radius = radius;
            DrawType = DrawType.Draw;
        }

        /// <summary>
        /// 渲染
        /// </summary>
        /// <param name="g"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public void Render(Graphics g)
        {
            switch (DrawType)
            {
                case DrawType.Fill:
                    Rectangle rIn = new Rectangle(X, Y, Radius, Radius);
                    g.FillEllipse(Brush, rIn);
                    break;
                case DrawType.Draw:
                    Rectangle rOut = new Rectangle(X, Y, Radius, Radius);
                    g.DrawEllipse(Pen, rOut);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }
    }

    internal enum DrawType
    {
        Fill,
        Draw
    }

}
